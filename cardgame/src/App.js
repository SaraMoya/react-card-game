import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {

const cards = [
  {character: 'Hel', weapon: 'Book of shadows', 'magicalResistance': 100, 'physicalResistance' : 25, 'magicalDamage' : 100, 'physicalDamage': 0},
  {character: 'Isis', weapon: 'Staff', 'magicalResistance': 70, 'physicalResistance' : 30, 'magicalDamage' : 45, 'physicalDamage': 0},
  {character: 'Kali', weapon: 'Energy sphere', 'magicalResistance': 85, 'physicalResistance' : 40, 'magicalDamage' : 60, 'physicalDamage': 0},
  {character: 'The morrigan', weapon: 'Double wands', 'magicalResistance': 65, 'physicalResistance' : 50, 'magicalDamage' : 55, 'physicalDamage': 0},
  {character: 'Persephone', weapon: 'Book of light', 'magicalResistance': 90, 'physicalResistance' : 20, 'magicalDamage' : 95, 'physicalDamage': 0},
  {character: 'Loki', weapon: 'Obsidian amulet', 'magicalResistance': 100, 'physicalResistance' : 15, 'magicalDamage' : 70, 'physicalDamage': 0},
  {character: 'Anubis', weapon: 'Axe', 'magicalResistance': 70, 'physicalResistance' : 50, 'magicalDamage' : 0, 'physicalDamage': 50},
  {character: 'Cernunnos', weapon: 'Mace', 'magicalResistance': 80, 'physicalResistance' : 40, 'magicalDamage' : 0, 'physicalDamage': 45},
  {character: 'Hades', weapon: 'Scythe', 'magicalResistance': 70, 'physicalResistance' : 30, 'magicalDamage' : 0, 'physicalDamage': 95},
  {character: 'Banshee', weapon: 'Ruby ring', 'magicalResistance': 35, 'physicalResistance' : 100, 'magicalDamage' : 55, 'physicalDamage': 0},
  {character: 'Dryad', weapon: 'Forest tiara', 'magicalResistance': 15, 'physicalResistance' : 60, 'magicalDamage' : 60, 'physicalDamage': 0},
  {character: 'Medusa', weapon: 'Enchanted gloves', 'magicalResistance': 30, 'physicalResistance' : 65, 'magicalDamage' : 80, 'physicalDamage': 0},
  {character: 'Siren', weapon: 'Strike whip', 'magicalResistance': 25, 'physicalResistance' : 70, 'magicalDamage' : 0, 'physicalDamage': 45},
  {character: 'Fenrir', weapon: 'Ring blade', 'magicalResistance': 45, 'physicalResistance' : 100, 'magicalDamage' : 0, 'physicalDamage': 80},
  {character: 'Vampyr', weapon: 'Sword and dagger', 'magicalResistance': 15, 'physicalResistance' : 90, 'magicalDamage' : 0, 'physicalDamage': 70},
  {character: 'Kelpie', weapon: 'chakrams', 'magicalResistance': 40, 'physicalResistance' : 50, 'magicalDamage' : 0, 'physicalDamage': 60},
  {character: 'Basilisk', weapon: 'Double swords', 'magicalResistance': 20, 'physicalResistance' : 25, 'magicalDamage' : 20, 'physicalDamage': 100},
  {character: 'Pele', weapon: 'Jormungandr bracelet', 'magicalResistance': 50, 'physicalResistance' : 40, 'magicalDamage' : 60, 'physicalDamage': 0},
  {character: 'character19', weapon: 'Bow', 'magicalResistance': 10, 'physicalResistance' : 70, 'magicalDamage' : 0, 'physicalDamage': 55},
  {character: 'character20', weapon: 'Crossbow', 'magicalResistance': 30, 'physicalResistance' : 65, 'magicalDamage' : 0, 'physicalDamage': 65}
]


  
  return (
    <div className="App">
      
    </div>
  );
}

export default App;


// const playerDeck = [0, 1, 11, 3, 13, 5, 15, 7, 17, 9]
// const computerDeck = [10, 2, 12, 4, 14, 6, 16, 8, 18, 19]
// const playerArena = []
// const computerArena = []

// const displayCards = (deck) => {
//     deck.map(ele => {
//         console.log(cards[ele])
//     })
// }

// var playerCardIndx 
// var playerCard  

// var pickACard = () => {
//     var userChoice = prompt(`pick a card`)
//     playerCardIndx  = userChoice
//     playerCard  = cards[playerDeck[playerCardIndx]]
//     console.log(playerCard)
// }

// var computerCardIndx = Math.round(Math.random())
// var computerCard = cards[computerDeck[computerCardIndx]]

// const compare = () => {
//     debugger
//     if (computerCard.magicalDamage > playerCard.magicalResistance) {
//         computerArena.push(computerCard)
//     }  else if (computerCard.magicalDamage < playerCard.magicalResistance) {
//         playerArena.push(playerCard)
//     } else if (computerCard.magicalDamage == playerCard.magicalResistance) {
//         if (computerCard.physicalDamage > playerCard.physicalResistance) {computerArena.push(computerCard)}
//         if (computerCard.physicalDamage < playerCard.physicalResistance ) {playerArena.push(playerCard)}
//     }

//     computerDeck.splice(computerCardIndx, 1) 
//     playerDeck.splice(playerCardIndx, 1)
//     return playerArena 
// }

// const winner = () => {
//     playerArena.length > computerArena.length ? console.log('Player wins!') : console.log('Computer wins')
// }
